title: man/df/python script for dataframe
tags: manpage

! Demonstration script: df2tiddler.py

```python
#! /usr/bin/python3

'''
Simple script to export a csv file through pandas df to a json tiddler.
'''

import sys, os
import pandas as pd
import json

import itertools

na_value = 'N/A'   # &nbsp;  seems a good candidate
description = None

def df2json( df, title, desc="", name="id"):
    payload = []
    # base dataframe
    text = '{{||tags/dataframe.template}}'
    if desc:
        text += '\n%s' % desc
    payload.append( { 'title' : title, 'tags': 'dataframe', 'text' : text, 'name': name })
    # index
    idx = list( range(len( df.index )) )
    payload.append( {'title': title + '.index',
                     'type' : 'application/json', 
                     'text' : json.dumps( dict( zip(idx , df.index) ) ) })
    # columns
    cls = list( range(len( df.columns )) )
    payload.append( {'title': title + '.columns',
                     'type' : 'application/json', 
                     'text' : json.dumps( dict( zip(cls, df.columns) ) ) })

    # values
    pdNAtype = type( pd.NA )
    value = lambda c: df.iloc[c] if not isinstance(df.iloc[c], pdNAtype) else na_value
    payload.append( {'title': title + '.values',
                     'type' : 'application/json', 
                     'text' : json.dumps( dict(map(lambda c: ('%d,%d' % c, value(c)), itertools.product(idx, cls) ) ) )
                     }) 
    return payload


def main(csvfile):
    # load csvfile as string
    (head, tail) = os.path.split( csvfile )
    (root, ext)  = os.path.splitext( tail )
    jsonfile = os.path.join( head, root) + '.json'
    df_id = root
    print('read csv file: %s' % csvfile)
    df = pd.read_csv( csvfile, dtype=pd.StringDtype(), sep=";" )

    payload=[]
    
    # store dataframe as is
    df_path = 'data/df/%s' % df_id
    payload.extend( df2json(df, df_path, description) )

    # re-index df following excel standards
    df_columns = pd.DataFrame( df.columns, index=df.columns ).T
    df_excel = pd.concat([ df_columns, df ], ignore_index=True)
    df_excel.columns = list( map( lambda i: chr( ord('A') + i), range( len(df_excel.columns) ) ) )
    df_excel.index = list( range(1, len( df_excel.index) + 1 ) )
    df_path = 'data/excel/%s' % df_id
    payload.extend( df2json(df_excel, df_path, description) )
    
    fh = open(jsonfile, 'w')
    json.dump( payload, fh)
    print('Wrote payload to: ', fh.name)
    fh.close()

if __name__ == '__main__':

    csvfile = None
    arg_i = 1
    while len(sys.argv) > arg_i:
        if sys.argv[arg_i].startswith('-na'):
            arg_i += 1
            na_value = sys.argv[arg_i]
        elif sys.argv[arg_i].startswith('-d'):
            arg_i += 1
            description = sys.argv[arg_i]
        elif sys.argv[arg_i].startswith('-h'):
            print('usage: df2tiddler [-na value] [-d description] csvfile', file=sys.stderr)
            break
        elif sys.argv[arg_i].startswith('-'):
            print('Unknown option: ' + sys.argv[arg_i], file=sys.stderr)
        else:
            csvfile = sys.argv[arg_i]
            break
        arg_i += 1
    
    if not csvfile:
        print('Missing csvfile.', file=sys.stderr)
        sys.exit(1)
        
    main(csvfile)
    
    
    
    
```
