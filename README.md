# tw-dataframe #

A simple plugin for tiddlywiki to display datframes in a table format.

## Why tw-dataframe ##

I consider using the [Tamasha](https://kookma.github.io/TW-Tamasha/) plugin for presentations.
Since I frequently use spreadsheets or other datagrams I was looking for a simple way to import
the latest version of the data into the slides.

This plugin does precisely that. One can transform a
[pandas dataframe](https://pandas.pydata.org/docs/reference/frame.html)
into a tuple of tiddler, conveniently packed in a .json file.

## How tw-dataframe ##

Here you find here the source files:

* plugins/* contains the plugin file that usually reside in TW5/plugins/hydiga/dataframe
* imports/* the json files with the example dataframes used in the documentation
* tiddlers/* the tiddlers used in the demonstration tiddlywiki

## Demonstration ##

A static demonstration is avaiable at: [bitbucket.io](https://hwvandijk.bitbucket.io/tw-dataframe/)

## Installing the plugin ##

You can drag and drop the plugin from [dataframe tiddlywiki](https://hwvandijk.bitbucket.io/tw-dataframe/).

* Open the configuration,
* select plugins, and
* drag and drop dataframe plugin.

